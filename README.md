# Examples-of-codes

---

This repo is for simple and educational codes in several different languages. I occasionally upload some code here. Our goal is to learn, let's learn!

---

## The languages with which we have written code so far.

---

### Whats C?

> C is a general-purpose, procedural computer programming language supporting structured programming, lexical variable scope, and recursion, with a static type system. By design, C provides constructs that map efficiently to typical machine instructions.

---

### Whats Erlang?

> Erlang is a general-purpose, concurrent, functional programming language, and a garbage-collected runtime system.

---

### Whats Rust?

> Rust is a modern systems programming language focusing on safety and speed.
> It accomplishes these goals by being memory safe without using garbage collection.
> “The Rust Programming Language” is split into three sections, which you can navigate through the menu on the left."

---

## examples:

* [Factorial](https://komeilparseh.github.io/Examples-of-codes/examples/Factorial/)
* [Fibonacci](https://komeilparseh.github.io/Examples-of-codes/examples/Fibonacci/)
* [find Prime Number](https://komeilparseh.github.io/Examples-of-codes/examples/is_prime/)
* [Pascal's triangle](https://komeilparseh.github.io/Examples-of-codes/examples/Pascaltriangle/)
* [Project Euler](https://komeilparseh.github.io/Examples-of-codes/Project%20Euler/)

---

## link's

Erlang Programing Labguage WebSite [here](http://erlang.org/)
Rust Programing language WebSite [here](https://rust-lang.org)
Run Rust Online on Your web Browser [here](https://play.rust-lang.org/)
